# Recipe App API Proxy
NGINX proxy app for our recipe app api 

## Usage

### Environment Variables

* 'LISTEN_PORT' - port to listen on (default: '8000')
* 'APP_HOST' - hostname of app to forward requests to (default: 'app')
* 'APP_PORT' - port of the aopp to forward requests to (default: '9000')
